"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const application_1 = require("@sgtools/application");
const console_1 = require("@sgtools/console");
require("@sgtools/console-notifications");
console_1.AppConsole.prototype.printTitle = (text = '') => {
    let message = application_1.app.description.toUpperCase() + (text === '' ? '' : ' - ' + text.toUpperCase());
    console_1.console.addPrivateColor('title', '1;30;47', '22;39;49');
    console_1.console.clear();
    console_1.console.printLine(`[title]${message}$$[/title]`);
    console_1.console.newline();
    console_1.console.notifications.show();
};
console_1.AppConsole.prototype.printSubTitle = (text) => {
    console_1.console.addPrivateColor('subtitle', '1;48;5;236', '22;49');
    console_1.console.printLine(`[subtitle]${text.toUpperCase()}$$[/subtitle]`);
};

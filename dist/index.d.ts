import '@sgtools/console-notifications';
declare module '@sgtools/console' {
    interface AppConsole {
        printTitle(text?: string): void;
        printSubTitle(text: string): void;
    }
}

import { app } from '@sgtools/application';

import { console, AppConsole } from '@sgtools/console';
import '@sgtools/console-notifications';

declare module '@sgtools/console'
{
    export interface AppConsole
    {
        printTitle(text?: string): void;
        printSubTitle(text: string): void;
    }
}
AppConsole.prototype.printTitle = (text: string = '') =>
{
    let message = app.description.toUpperCase() + (text === '' ? '' : ' - ' + text.toUpperCase());

    console.addPrivateColor('title', '1;30;47', '22;39;49');
    console.clear();
    console.printLine(`[title]${message}$$[/title]`);
    console.newline();
    console.notifications.show();
}
AppConsole.prototype.printSubTitle = (text: string) =>
{
    console.addPrivateColor('subtitle', '1;48;5;236', '22;49');
    console.printLine(`[subtitle]${text.toUpperCase()}$$[/subtitle]`);
}
